require('../lib');

var Style = require('../lib/Style');
var fnColor,
    fnStyle,
    padLength = 15,
    padChar = ' ',
    str = ['normal'.padRight(padLength, padChar).toBold()];

Style.getStyles(true).forEach(function (style) {
    str.push(style.padRight(padLength, padChar).toBold());
});

console.log(str.join(''));

Style.getColors().forEach(function (color) {
    fnColor = 'to' + color.ucfirst();
    str = [];
    color = color.padRight(padLength, padChar);

    str.push(color[fnColor]());

    Style.getStyles(true).forEach(function (style) {
        fnStyle = 'to' + style.ucfirst();
        str.push(color[fnColor]()[fnStyle]());
    });

    console.log(str.join(''));
});
