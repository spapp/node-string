node-string
c 2013 Sandor Papp
Sandor Papp <spapp@spappsite.hu>
http://spappsite.hu
https://bitbucket.org/spapp

CONTENTS
I. DESCRIPTION
II. REQUIRED
III. GETTING STARTED
IV. LICENSE


I. DESCRIPTION

This module supplements the String object with some function.
For example,
    - it is easy to work with the colors,
    - pad a string to a certain length with another string,
    - make a string's first character uppercase.


II. REQUIRED

See package.json


III. GETTING STARTED

In your node.js code:

require('node-string');


// use colors
"my color text".toRed().toBold();       // Red and bold foreground.
"my color text".toRed().toInverse();    // Inverse red text.
"my color text".toRedBg().toGreen();    // Red background and green foreground.

// ucfirst
console.log('string'.ucfirst()); // String

// pad
console.log('123'.pad(5, '0', String.PAD_LEFT)); // 00123


V. LICENSE

The MIT License (MIT)

Copyright (c) 2013 Sandor Papp <spapp@spappsite.hu>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.