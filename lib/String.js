if ('function' !== typeof(String.isString)) {
    /**
     * Returns true if params is a string, otherwise returns false.
     *
     * @param {*} object
     * @returns {boolean}
     * @function
     */
    String.isString = function String_isString (object) {
        return ('string' === typeof(object));
    };
}
/**
 * Left padding flag.
 *
 * @type {string}
 * @constant
 */
String.PAD_LEFT = 'left';
/**
 * Right padding flag.
 *
 * @type {string}
 * @constant
 */
String.PAD_RIGHT = 'right';

Object.defineProperty(String.prototype, 'ucfirst', {
    enumerable:   false,
    configurable: false,
    writable:     false,
    /**
     * Make a string's first character uppercase.
     *
     * @returns {String}
     * @type {Function}
     * @function
     */
    value:        function String_ucfirst () {
        return (this.substr(0, 1).toUpperCase() + this.substr(1));
    }
});

Object.defineProperty(String.prototype, 'pad', {
    enumerable:   false,
    configurable: false,
    writable:     false,
    /**
     * Pad a string to a certain length with another string.
     *
     * If the value of padLength is negative, less than,
     * or equal to the length of the string, no padding takes place.
     *
     * @param {number} padLength new string length
     * @param {string} padChar padding character
     * @param {string} padType padding type; {@link PAD_LEFT} or {@link PAD_RIGHT}
     * @returns {String}
     * @type {Function}
     * @function
     */
    value:        function String_pad (padLength, padChar, padType) {
        var str = this._original || this;
        var pad = '';

        if (padLength > str.length && 'string' === typeof(padChar)) {
            pad = new Array(padLength - str.length + 1).join(padChar.charAt(0));

            if (padType === String.PAD_LEFT) {
                str = pad + str;
            } else if (padType === String.PAD_RIGHT) {
                str = str + pad;
            }

            return str;
        }

        return this;
    }
});

Object.defineProperty(String.prototype, 'padLeft', {
    enumerable:   false,
    configurable: false,
    writable:     false,
    /**
     * Pad a string to a certain length with another string on the left side.
     *
     * @see {@link pad}
     * @param {number} padLength new string length
     * @param {string} padChar padding character
     * @returns {String}
     * @type {Function}
     * @function
     */
    value:        function String_padLeft (padLength, padChar) {
        return this.pad(padLength, padChar, String.PAD_LEFT);
    }
});

Object.defineProperty(String.prototype, 'padRight', {
    enumerable:   false,
    configurable: false,
    writable:     false,
    /**
     * Pad a string to a certain length with another string on the right side.
     *
     * @see {@link pad}
     * @param {number} padLength new string length
     * @param {string} padChar padding character
     * @returns {String}
     * @type {Function}
     * @function
     */
    value:        function String_padRight (padLength, padChar) {
        return this.pad(padLength, padChar, String.PAD_RIGHT);
    }
});
