var Style = require('./Style.js');
/**
 * The fallback style.
 *
 * @type {Style}
 */
var defaultStyle = new Style(Style.DEFAULT_COLOR);
/**
 * Returns a function that handles the current String object style.
 *
 * @this {String}
 * @param {String} fnName function name in Style object
 * @param {*} [value] function argument
 * @returns {Function}
 */
function styleFactory (fnName, value) {
    return function () {
        if (!this._style) {
            this._style = new Style();
        }

        if (!this._original) {
            this._original = this;
        }

        if ('undefined' === typeof(value)) {
            this._style[fnName]();
        } else {
            this._style[fnName](value);
        }

        return this.toString();
    };
}
/**
 * Add some function to String prototype.
 *
 * @see {@link styleFactory}
 * @param {Array} props setter names
 * @param {String} fn function name in Style object
 * @returns {void}
 */
function makeSetters (props, fn) {
    props.forEach(function (value, index) {
        var fnName = 'to';
        if ('string' === typeof(value)) {
            fnName += value.ucfirst();

            Object.defineProperty(String.prototype, fnName, {
                enumerable:   false,
                configurable: false,
                writable:     false,
                value:        styleFactory(fn, index)
            });
        }
    });
}
/**
 * Sets foreground high intensity.
 *
 * @see {@link Style#high}
 * @returns {this}
 */
Object.defineProperty(String.prototype, 'toHigh', {
    enumerable:   false,
    configurable: false,
    writable:     false,
    value:        styleFactory('high')
});
/**
 * Sets foreground normal intensity.
 *
 * @see {@link Style#low}
 * @returns {this}
 */
Object.defineProperty(String.prototype, 'toLow', {
    enumerable:   false,
    configurable: false,
    writable:     false,
    value:        styleFactory('low')
});
/**
 * Sets background high intensity.
 *
 * @see {@link Style#highBg}
 * @returns {this}
 */
Object.defineProperty(String.prototype, 'toHighBg', {
    enumerable:   false,
    configurable: false,
    writable:     false,
    value:        styleFactory('highBg')
});
/**
 * Sets background normal intensity.
 *
 * @see {@link Style#lowBg}
 * @returns {this}
 */
Object.defineProperty(String.prototype, 'toLowBg', {
    enumerable:   false,
    configurable: false,
    writable:     false,
    value:        styleFactory('lowBg')
});
/**
 * Clear all styles and colors.
 *
 * @see {@link Style#reset}
 * @returns {this}
 */
Object.defineProperty(String.prototype, 'resetStyle', {
    enumerable:   false,
    configurable: false,
    writable:     false,
    value:        styleFactory('reset')
});

Object.defineProperty(String.prototype, 'toString', {
    enumerable:   false,
    configurable: false,
    writable:     false,
    /**
     * Returns string value.
     *
     * @returns {string}
     */
    value:        function () {
        var escapeText = '';

        if (this._style) {
            escapeText = this._style.toString();
        }

        if (0 < escapeText.length) {
            return escapeText + this._original + defaultStyle.toString();
        }

        return this;
    }
});

makeSetters(Style.getColors(), 'setColor');
makeSetters(Style.getStyles(), 'setStyle');