/**
 * Console color list.
 *
 * The index is equal the color value.
 *
 * @type {Array}
 */
var colors = ['black', 'red', 'green', 'yellow', 'blue', 'purple', 'cyan', 'white'];
/**
 * Console style list.
 *
 * The index is equal the style value.
 *
 * @type {Array}
 */
var styles = [null, 'bold', null, 'italic', 'underline', null, null, 'inverse', null, 'strikethrough'];
/**
 * The foreground color base value.
 *
 * @type {number}
 * @constant
 */
var COLOR_BASE = 30;
/**
 * The foreground color high intensity base value.
 *
 * @type {number}
 * @constant
 */
var COLOR_HIGH_BASE = 90;
/**
 * The background color base value.
 *
 * @type {number}
 * @constant
 */
var COLOR_BG_BASE = 40;
/**
 * The background color high intensity base value.
 *
 * @type {number}
 * @constant
 */
var COLOR_BG_HIGH_BASE = 100;
/**
 * The escape character, which causes to take some action.
 *
 * @type {string}
 * @constant
 */
var ESC = '\u001b';
/**
 * The escape sequence creator class.
 *
 * @type {Function}
 * @constructor
 * @class
 */
var Style = module.exports = function Style (color) {
    this.setColor(color);
};
/**
 * Color constant value.
 *
 * COLOR_{color name} for example: COLOR_RED
 *
 * @see {@link colors}
 * @type {number}
 * @constant
 */
colors.forEach(function (color, index) {
    var name = 'COLOR_' + color.toUpperCase();
    Style[name] = index;
});
/**
 * Style constant value.
 *
 * STYLE_{style name} for example: STYLE_BOLD
 *
 * @see {@link styles}
 * @type {number}
 * @constant
 */
styles.forEach(function (style, index) {
    var name = 'STYLE_';
    if (style) {
        name += style.toUpperCase();
        Style[name] = index;
    }
});
/**
 * Fallback color flag.
 *
 * @type {number}
 * @constant
 */
Style.DEFAULT_COLOR = -1;
/**
 * Current foreground color value.
 *
 * @type {null|number}
 * @private
 */
Style.prototype._color = null;
/**
 * Current background color value.
 *
 * @type {null|number}
 * @private
 */
Style.prototype._bg = null;
/**
 * Current style value.
 *
 * @type {null|number}
 * @private
 */
Style.prototype._style = null;
/**
 * Foreground intensity flag.
 *
 * @type {boolean}
 * @private
 */
Style.prototype._high = false;
/**
 * Background intensity flag.
 *
 * @type {boolean}
 * @private
 */
Style.prototype._highBg = false;
/**
 * Returns all color names.
 *
 * @returns {Array}
 */
Style.getColors = function Style_getColors () {
    return colors;
};
/**
 * Returns all style names.
 *
 * If prepared is true then returns without placeholder values.
 *
 * @param {Boolean} [prepared]
 * @returns {Array}
 */
Style.getStyles = function Style_getStyles (prepared) {
    if (true === prepared) {
        return styles.filter(function (style) {
            return !!style;
        });
    }
    return styles;
};

Object.defineProperty(Style.prototype, 'setColor', {
    enumerable:   true,
    configurable: false,
    writable:     false,
    /**
     * Sets foreground color.
     *
     * @param {string|number} color color name or index
     * @returns {this}
     */
    value:        function Style_setColor (color) {
        color = (color === Style.DEFAULT_COLOR) ? Style.DEFAULT_COLOR : getIndex(color, Style.getColors());

        if (null !== color) {
            this._color = color;
        }
        return this;
    }
});

Object.defineProperty(Style.prototype, 'setBg', {
    enumerable:   true,
    configurable: false,
    writable:     false,
    /**
     * Sets background color.
     *
     * @param {string|number} color color name or index
     * @returns {this}
     */
    value:        function Style_setBg (color) {
        color = getIndex(color, Style.getColors());

        if (null !== color) {
            this._bg = color;
        }

        return this;
    }
});

Object.defineProperty(Style.prototype, 'setStyle', {
    enumerable:   true,
    configurable: false,
    writable:     false,
    /**
     * Sets style.
     *
     * @param {string|number} style style name or index
     * @returns {this}
     */
    value:        function Style_setStyle (style) {
        style = getIndex(style, Style.getStyles());

        if (null !== style) {
            this._style = style;
        }

        return this;
    }
});

Object.defineProperty(Style.prototype, 'reset', {
    enumerable:   true,
    configurable: false,
    writable:     false,
    /**
     * Clear all styles and colors.
     *
     * @returns {this}
     */
    value:        function Style_reset () {
        this._color = null;
        this._bg = null;
        this._style = null;
        this._high = false;
        this._highBg = false;

        return this;
    }
});

Object.defineProperty(Style.prototype, 'high', {
    enumerable:   true,
    configurable: false,
    writable:     false,
    /**
     * Sets foreground high intensity.
     *
     * @returns {this}
     */
    value:        function Style_high () {
        this._high = true;
        return this;
    }
});

Object.defineProperty(Style.prototype, 'low', {
    enumerable:   true,
    configurable: false,
    writable:     false,
    /**
     * Sets foreground normal intensity.
     *
     * @returns {this}
     */
    value:        function Style_low () {
        this._high = false;

        return this;
    }
});

Object.defineProperty(Style.prototype, 'highBg', {
    enumerable:   true,
    configurable: false,
    writable:     false,
    /**
     * Sets background high intensity.
     *
     * @returns {this}
     */
    value:        function Style_highBg () {
        this._highBg = true;

        return this;
    }
});

Object.defineProperty(Style.prototype, 'lowBg', {
    enumerable:   true,
    configurable: false,
    writable:     false,
    /**
     * Sets background normal intensity.
     *
     * @returns {this}
     */
    value:        function Style_lowBg () {
        this._highBg = false;
        return this;
    }
});

Object.defineProperty(Style.prototype, 'toString', {
    enumerable:   true,
    configurable: false,
    writable:     false,
    /**
     * Creates escape-code.
     *
     * @returns {string}
     */
    value:        function Style_toString () {
        var style = [];
        var base = (true === this._high) ? COLOR_HIGH_BASE : COLOR_BASE;
        var bgBase = (true === this._highBg) ? COLOR_BG_HIGH_BASE : COLOR_BG_BASE;

        if ('number' === typeof(this._style)) {
            style.push(this._style);
        }

        if ('number' === typeof(this._color)) {
            if (0 > this._color) {
                style.push(0);
            } else {
                style.push(base + this._color);
            }
        }

        if ('number' === typeof(this._bg)) {
            style.push(bgBase + this._bg);
        }

        if (0 < style.length) {
            style = ESC + '[' + style.join(';') + 'm';
        } else {
            style = '';
        }

        return style;
    }
});
/**
 * Returns index of 'search' in the 'source' array.
 *
 * @private
 * @param {string|number} search
 * @param {Array} source
 * @returns {number|null}
 */
function getIndex (search, source) {
    if ('string' === typeof(search)) {
        search = source.indexOf(search);
    }

    if ('number' !== typeof(search) || 0 > search || search >= source.length) {
        search = null;
    }

    return search;
}
