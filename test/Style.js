var assert = require('assert');
var Style = require('../lib/Style');

describe('Style', function () {
    var style;
    var options = {
        style:      Style.STYLE_BOLD,   // 1
        color:      Style.COLOR_RED,    // 1
        bg:         Style.COLOR_YELLOW, // 3
        high:       true,               // 90
        highBg:     true,               // 100
        escapeCode: '\u001b[1;91;103m'
    };

    function getAStyle (options) {
        var style = new Style();

        style.setColor(options.color);
        style.setBg(options.bg);
        style.setStyle(options.style);

        if (options.high) {
            style.high();
        }

        if (options.highBg) {
            style.highBg();
        }

        return style
    }

    beforeEach(function () {
        style = new Style();
    });

    describe('#setColor', function () {
        it('function is exists', function () {
            assert.equal('function', typeof(Style.prototype.setColor));
        });

        it('default value is null', function () {
            assert.equal(null, style._color);
        });

        it('foreground color is red', function () {
            style.setColor(Style.COLOR_RED);
            assert.equal(Style.COLOR_RED, style._color);
        });

        it('foreground color is blue', function () {
            style.setColor(Style.COLOR_BLUE);
            assert.equal(Style.COLOR_BLUE, style._color);
        });
    });

    describe('#setBg', function () {
        it('function is exists', function () {
            assert.equal('function', typeof(Style.prototype.setBg));
        });

        it('default value is null', function () {
            assert.equal(null, style._bg);
        });

        it('background color is red', function () {
            style.setBg(Style.COLOR_RED);
            assert.equal(Style.COLOR_RED, style._bg);
        });

        it('background color is blue', function () {
            style.setBg(Style.COLOR_BLUE);
            assert.equal(Style.COLOR_BLUE, style._bg);
        });
    });

    describe('#setStyle', function () {
        it('function is exists', function () {
            assert.equal('function', typeof(Style.prototype.setStyle));
        });

        it('default value is null', function () {
            assert.equal(null, style._style);
        });

        it('style is bold', function () {
            style.setStyle(Style.STYLE_BOLD);
            assert.equal(Style.STYLE_BOLD, style._style);
        });

        it('style is inverse', function () {
            style.setStyle(Style.STYLE_INVERSE);
            assert.equal(Style.STYLE_INVERSE, style._style);
        });
    });

    describe('#high', function () {
        it('function is exists', function () {
            assert.equal('function', typeof(Style.prototype.high));
        });
        it('default value is false', function () {
            assert.equal(false, style._high);
        });
        it('foreground color is high intensity', function () {
            style.high();
            assert.equal(true, style._high);
        });
    });

    describe('#low', function () {
        it('function is exists', function () {
            assert.equal('function', typeof(Style.prototype.low));
        });

        it('foreground color is normal intensity', function () {
            style.low();
            assert.equal(false, style._high);
        });
    });
    describe('#highBg', function () {
        it('function is exists', function () {
            assert.equal('function', typeof(Style.prototype.highBg));
        });
        it('default value is false', function () {
            assert.equal(false, style._highBg);
        });
        it('background color is high intensity', function () {
            style.highBg();
            assert.equal(true, style._highBg);
        });
    });
    describe('#lowBg', function () {
        it('function is exists', function () {
            assert.equal('function', typeof(Style.prototype.lowBg));
        });

        it('background color is normal intensity', function () {
            style.lowBg();
            assert.equal(false, style._high);
        });
    });

    describe('#reset', function () {
        var style1 = getAStyle(options);

        it('function is exists', function () {
            assert.equal('function', typeof(Style.prototype.reset));
        });

        it('style is not equal the default values', function () {
            assert.equal(options.color, style1._color);
            assert.equal(options.bg, style1._bg);
            assert.equal(options.style, style1._style);
            assert.equal(options.high, style1._high);
            assert.equal(options.highBg, style1._highBg);
        });

        it('style is equal the default values', function () {
            style1.reset();
            assert.equal(null, style1._color);
            assert.equal(null, style1._bg);
            assert.equal(null, style1._style);
            assert.equal(false, style1._high);
            assert.equal(false, style1._highBg);
        });
    });

    describe('#toString', function () {
        it('function is exists', function () {
            assert.equal('function', typeof(Style.prototype.toString));
        });
        it('if not set style then not return escape-code', function () {
            assert.equal('', style.toString());
            assert.equal(0, style.toString().length);
        });
        it('if set style then return escape-code', function () {
            var style1 = getAStyle(options);
            assert.equal(options.escapeCode, style1.toString());
            assert.equal(options.escapeCode.length, style1.toString().length);
        });
    });
});