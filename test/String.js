var assert = require('assert');
var utils = require('../lib');

describe('String', function () {

    describe('#ucfirst', function () {
        it('first character uppercase', function () {
            assert.equal('Aaaaa', 'aaaaa'.ucfirst());
            assert.equal('AAAAA', 'AAAAA'.ucfirst());
            assert.equal('Aaaaa bbb', 'aaaaa bbb'.ucfirst());
            assert.equal('AAAAA BBB', 'AAAAA BBB'.ucfirst());
            assert.equal('Aaaaa Bbb', 'aaaaa Bbb'.ucfirst());
        });
    });

    describe('#pad', function () {
        var str = 'aaaaa';
        var padChar = 'X';

        it('missing params', function () {
            assert.equal(str, str.pad(110, padChar));
            assert.equal(str, str.pad(10));
            assert.equal(str, str.pad());
        });

        it('the value of padLength is negative, less than, or equal to the length of the string', function () {
            assert.equal(str, str.pad(-10, padChar, String.PAD_LEFT));
            assert.equal(str, str.pad(-1, padChar, String.PAD_RIGHT));
            assert.equal(str, str.pad(3, padChar, String.PAD_RIGHT));
            assert.equal(str, str.pad(2, padChar, String.PAD_RIGHT));
            assert.equal(str, str.pad(5, padChar, String.PAD_RIGHT));
        });

        it('left padding', function () {
            assert.equal('00123', '123'.pad(5, '0', String.PAD_LEFT));
            assert.equal('12345', '12345'.pad(5, '0', String.PAD_LEFT));
            assert.equal('123456', '123456'.pad(5, '0', String.PAD_LEFT));

            assert.equal('XXaaa', 'aaa'.pad(5, 'X', String.PAD_LEFT));
            assert.equal('aaaaa', 'aaaaa'.pad(5, 'X', String.PAD_LEFT));
            assert.equal('aaaaaa', 'aaaaaa'.pad(5, 'X', String.PAD_LEFT));
        });

        it('right padding', function () {
            assert.equal('12300', '123'.pad(5, '0', String.PAD_RIGHT));
            assert.equal('12345', '12345'.pad(5, '0', String.PAD_RIGHT));
            assert.equal('123456', '123456'.pad(5, '0', String.PAD_RIGHT));

            assert.equal('aaaXX', 'aaa'.pad(5, 'X', String.PAD_RIGHT));
            assert.equal('aaaaa', 'aaaaa'.pad(5, 'X', String.PAD_RIGHT));
            assert.equal('aaaaaa', 'aaaaaa'.pad(5, 'X', String.PAD_RIGHT));
        });
    });

    describe('#padLeft', function () {
        it('left padding', function () {
            assert.equal('00123', '123'.pad(5, '0', String.PAD_LEFT));
            assert.equal('12345', '12345'.pad(5, '0', String.PAD_LEFT));
            assert.equal('123456', '123456'.pad(5, '0', String.PAD_LEFT));

            assert.equal('XXaaa', 'aaa'.pad(5, 'X', String.PAD_LEFT));
            assert.equal('aaaaa', 'aaaaa'.pad(5, 'X', String.PAD_LEFT));
            assert.equal('aaaaaa', 'aaaaaa'.pad(5, 'X', String.PAD_LEFT));
        });
    });

    describe('#padRight', function () {
        it('right padding', function () {
            assert.equal('12300', '123'.pad(5, '0', String.PAD_RIGHT));
            assert.equal('12345', '12345'.pad(5, '0', String.PAD_RIGHT));
            assert.equal('123456', '123456'.pad(5, '0', String.PAD_RIGHT));

            assert.equal('aaaXX', 'aaa'.pad(5, 'X', String.PAD_RIGHT));
            assert.equal('aaaaa', 'aaaaa'.pad(5, 'X', String.PAD_RIGHT));
            assert.equal('aaaaaa', 'aaaaaa'.pad(5, 'X', String.PAD_RIGHT));
        });
    });

    describe('#isString', function () {
        it('function is exists', function () {
            assert.equal('function', typeof(String.isString));
        });

        it('is a string', function () {
            assert.strictEqual(true, String.isString('sssdfdffd'));
            assert.strictEqual(true, String.isString('1234'));
            assert.strictEqual(true, String.isString('123.111'));
            assert.strictEqual(true, String.isString('[]'));
            assert.strictEqual(true, String.isString('x332322'));
        });

        it('is not a string', function () {
            assert.strictEqual(false, String.isString(123));
            assert.strictEqual(false, String.isString(12.11));
            assert.strictEqual(false, String.isString([]));
            assert.strictEqual(false, String.isString(/abc/));
            assert.strictEqual(false, String.isString({}));
            assert.strictEqual(false, String.isString(true));
            assert.strictEqual(false, String.isString(false));
            assert.strictEqual(false, String.isString(null));
            assert.strictEqual(false, String.isString(function () {
            }));
            assert.strictEqual(false, String.isString(new Date()));
        });
    });

    require('./color');

});