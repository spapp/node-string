var assert = require('assert');
var utils = require('../lib');
var Style = require('../lib/Style');

function functionIsExists (name) {
    describe('#' + name, function () {
        it('function is exists', function () {
            assert.equal('function', typeof(String.prototype[name]));
        });
    });
}

describe('colorization', function () {
    Style.getColors().forEach(function (color) {
        functionIsExists('to' + color.ucfirst());
    });

    Style.getStyles(true).forEach(function (style) {
        functionIsExists('to' + style.ucfirst());
    });

    ['toHigh', 'toLow', 'toHighBg', 'toLowBg', 'resetStyle'].forEach(function (fnName) {
        functionIsExists(fnName);
    });
});