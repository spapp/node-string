all: clean install test

test:
	./node_modules/.bin/mocha

install:
	npm install

examples:
	node ./example/console.js

clean:
	rm -rf ./node_modules

.PHONY: all clean install examples test
